import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # для отрисовки 3-д проекции

matplotlib.rc("font", size=18)  # для увеличения шрифта подписей графиков

# загружаем данные
houses = pd.read_csv("1.4_houses.csv")

print(houses.head(50))

fig = plt.figure(figsize=(10, 10))  # создаем картинку
ax = plt.axes()
ax.scatter(houses["dim_2"], houses["price"], s=100)  # помещаем точки на график, s - это размер точек
plt.show()

# Линейная регрессия f(x) = a * x + b -> price(dim_1) = a * dim_1 + b | a + b * dim_1

# Импортируем модуль, отвечающий за линейную регрессию
from sklearn.linear_model import LinearRegression

# Выгружаем признаки и целевые значения в отдельные переменные
X = houses[["dim_1"]]
print(X)
y = houses["price"]


# создаем регрессор
reg = LinearRegression().fit(X, y)
print(reg.coef_) # коэфициент а
print(reg.intercept_) # атрибут b

# Вытаскиваем нужные коэффициенты
[b] = reg.coef_
a = reg.intercept_

# создаем функцию для предсказания цены дома
def reg_prediction(dim_1):
    return a+b*dim_1

print(reg_prediction(houses.dim_1[0]))

# Используем встроенный метод для расчета предсказаний
print(reg.predict(X[0:1])[0])

fig = plt.figure(figsize=(10, 10)) # Создаем картинку
ax = plt.axes()

# Помещаем точки на график
ax.scatter(houses['dim_1'], houses["price"], s = 100)
# Помещаем предсказание
ax.plot([X.dim_1.min(), X.dim_1.max()], [reg_prediction(X.dim_1.min()), reg_prediction(X.dim_1.max())], c= "red")

# Отображаем картинку
plt.show()

# Отображение точенного графика на 3D проекцию
fig = plt.figure(figsize=(10, 10)) # создаем картинку
ax = plt.axes(projection = "3d") # устанавливаем проекцию

# Помещаем точки на гарфик
ax.scatter(houses["dim_1"], houses["price"], s = 100)
# Помещаем предсказание
ax.plot([X.dim_1.min(), X.dim_1.max()], [reg_prediction(X.dim_1.min()), reg_prediction(X.dim_1.max())], c = "red")

# называем оси
ax.set_xlabel("dim_1")
ax.set_ylabel("price")

# Отображаем картинку
plt.show()



# импотируем модуль отвечающий за лиинейную регрессию
from sklearn.linear_model import LinearRegression

# Выгружаем признаки и целевые значения
X = houses[["dim_1", "dim_2"]]
y = houses["price"]

# создаем регрессор
reg = LinearRegression().fit(X, y)

# Вытаскиваем нужные коэффициенты
[b1, b2] = reg.coef_
a = reg.intercept_

# Создаем функцию для предсказания цены дома
def reg_prediction(dim_1, dim_2):
    return a + b1*dim_1 + b2*dim_2

# Сделаем предсказания для различных конфигураций домов
d1, d2 = list(), list()
for x in np.linspace(min(houses["dim_1"]), max(houses["dim_1"]), 100):
    for y in np.linespace(min(houses["dim_2"]), max(house["dim_2"]), 100):
        d1.append(X)
        d2.append(y)
d1 = np.array(d1).reshape(-1, 1)
d2 = np.array(d2).reshape(-1, 1)
p = reg.predict(np.concatenate([d1, d2], axis = 1))

fig = plt.figure(figsize=(10, 10)) # создаем картинку
ax = plt.axes(projection="3d") # устанавливаем проекцию

# Помещаем точки на график
ax.plot_trisurf(d1.ravel(), d2.ravel(), p.ravel(), alpha = 0.2)

# назвываем оси
ax.set_xlabel("dim_1")
ax.set_ylabel('dim_2')
ax.set_zlabel("price")

# изменяем позицию камеры и трисовываем картинку
ax.elev = 27
plt.show()


