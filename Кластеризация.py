import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc("font", size = 18) # для увеличения шрифта подписей

# Загружаем данные
houses = pd.read_csv("1.4_houses.csv")

fig = plt.figure(figsize=(10, 10)) # создаем картинку

markers = {"basic": "o", "medium": "v", "luxury": "*"}
for d1, d2, L in zip(houses.dim_1, houses.dim_2, houses.level):
    plt.scatter(d1, d2, s = 250, marker = markers[L])

print(plt.show())

# Импортируем модуль, отвечающий за кластеризацию
from sklearn.cluster import KMeans

# Выгружаем данные в отдельную переменную
x = houses[["dim_1", "dim_2"]]

# Создаем модель для кластеризации
clust = KMeans(n_clusters = 3).fit(x)

# Смортрим центры кластеров
[c1, c2, c3] = clust.cluster_centers_


fig = plt.figure(figsize=(10, 10)) # Создаем картинку

for d1, d2, L in zip(houses.dim_1, houses.dim_2, houses.level):
    plt.scatter(d1, d2, marker=markers[L])

# Добавляем информацию о центрах кластеров
plt.scatter(c1[0], c1[1], s = 250, marker="x", c = 'black')
plt.scatter(c1[0], c1[1], s = 250 * 1e2, c = 'black', alpha=0.1)

plt.scatter(c2[0], c2[1], s = 250, marker='x', c = 'black')
plt.scatter(c2[0], c2[1], s = 250 * 1e2, c = "black", alpha=0.1)

plt.scatter(c3[0], c3[1], s = 250, marker="x", c = 'black')
plt.scatter(c3[0], c3[1], s = 250 * 4.0e2, c = "black", alpha=0.1)




# Отображаем картинку
plt.show()
