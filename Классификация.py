import numpy as np
import pandas as pd

# загружаем данные
houses = pd.read_csv("1.4_houses.csv")

print(houses.head(20))

# Импортируем модуль, отвечающий за деревья решений
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_text

# Выгружаем признаки и целевые значения в отдельные переменные
X = houses[["dim_1", "dim_2"]]
y = houses["level"]

# Создаем классификатор
cl = DecisionTreeClassifier().fit(X, y)

# Выдаем информацию для интерпритации построенной модели
print(export_text(cl))

# Проверяем классификацию
print(cl.predict(X[15:16])[0], y[15])
