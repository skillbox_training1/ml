import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Загрузка данных
houses = pd.read_csv("1.4_houses.csv")
print(houses.head())
houses.info()

""" Создали пустой (с нулями) DF с названием колонок для подсчета количества цифр в числах "price"
    датафрейма 'house' """
df_1 = pd.DataFrame(np.zeros((100, 10)))
df_1.columns = ['number_'+str(i) for i in range(10)]

#   Считаем сколько всего цифр встречается в каждом числе построчно
#   Заметим, что когда берем число, то мы конвертируем его из float в int, так как получается
# слишком много нулей после запятой.

for rowDF in range (100):
    x = int(houses.iloc[rowDF, 3])
    x = [a for a in str(x)]
    for i in range(len(x)):
        if x[i] == '0':
            df_1.iloc[rowDF, 0] += 1
        elif x[i] == '1':
            df_1.iloc[rowDF, 1] += 1
        elif x[i] == '2':
            df_1.iloc[rowDF, 2] += 1
        elif x[i] == '3':
            df_1.iloc[rowDF, 3] += 1
        elif x[i] == '4':
            df_1.iloc[rowDF, 4] += 1
        elif x[i] == '5':
            df_1.iloc[rowDF, 5] += 1
        elif x[i] == '6':
            df_1.iloc[rowDF, 6] += 1
        elif x[i] == '7':
            df_1.iloc[rowDF, 7] += 1
        elif x[i] == '8':
            df_1.iloc[rowDF, 8] += 1
        elif x[i] == '9':
            df_1.iloc[rowDF, 9] += 1
        else:
            continue

# Суммирование строк по столбцу, т.е. итоговое значение по столбцу.
x = (df_1.sum(axis = 0))
y = sum(x)
result = []
for i in range(len(x)):
    result.append(x[i]/y*100)

# Построение круговой диаграммы
fig1, ax1 = plt.subplots()
ax1.pie(result, labels=df_1.columns, autopct='%1.0f%%', )
ax1.axis('equal')
ax1.set_title("how many times the numbers occur in the 'Price' column ")
plt.show()

