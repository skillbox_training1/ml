import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

phones = pd.read_csv("1.8_phones.csv")
print(phones.head(50))

# Импортируем модуль отвечающий за Линейную регрессию
from sklearn.linear_model import LinearRegression

# Выгружаем признаки и целевые значения
x = phones[["disk", "year"]]
y = phones["price"]

# Создаем регрессию
reg = LinearRegression().fit(x, y)

# Создаем предсказание для различных телефонов в зависимости от объема памяти и года производства
x1, x2 = list(), list()
for x in np.linspace(min(phones["disk"]), max(phones["disk"]), 99):
    for y in np.linspace(min(phones["year"]), max(phones['year']), 99):
        x1.append(x)
        x2.append(y)
x1 = np.array(x1).reshape(-1, 1)
x2 = np.array(x2).reshape(-1, 1)
p = reg.predict(np.concatenate([x1, x2], axis = 1))

fig = plt.figure(figsize = (8, 6)) # создаем картинку
ax = plt.axes(projection = "3d") # Устанавливаем проекцию

# Помещаем точки на график
ax.plot_trisurf(x1.ravel(), x2.ravel(), p.ravel(), alpha=0.2)

# Называем оси
ax.set_xlabel("disk")
ax.set_ylabel("year")
ax.set_zlabel('price')
ax.set_title("Цена телефона в зависимости от года производства и объема памяти")

# Изменяем позиции камеры и отрисовываем картинку
ax.elev = 27
plt.show()


# Классификация
# Импортируем модуль, отвечающий за деревья решений
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_text

x = phones[["disk", "price"]]
y = phones["os"]

# Создам классификатор
cl = DecisionTreeClassifier().fit(x, y)

# Выдаем информацию для интерпретации построенной модели
print(export_text(cl))

# Проверяем классификацию
print(cl.predict(x[15:16])[0], y[15])


# Кластеризация
# Имортируем модуль, отвечающий за кластеризацию
from sklearn.cluster import KMeans

# Выгружаем данные в отдельную переменную
x = phones[["price", "disk"]]

# Создаем модель для кластеризации
clust = KMeans(n_clusters=2).fit(x)

# Смотрим центры кластеров
markers = {"Android": "o", "iOS":"*"}
[c1, c2] = clust.cluster_centers_
fig = plt.figure(figsize=(10, 10)) # Создаем картинку
for p, d, module in zip(phones.price, phones.disk, phones.os):
    plt.scatter(p, d, marker = markers[module])

plt.title("Кластеризация по \"os\"")
plt.xlabel("price")
plt.ylabel("disk")
plt.legend()

plt.show()




